package com.sample.fcmnotifications;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * FCMNotifications
 * Created by Administrator on 04-11-2018.
 */
public class MessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("Message", remoteMessage.toString());
    }

    @Override
    public void onNewToken(String s) {
        Log.e("Token", s);
        super.onNewToken(s);

    }
}
